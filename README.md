# FenNec2

<img src="images/logo.png" alt="Placeholder logo" width="200"/>

## What's this ?

FenNec2 is a Python-written tool built to provide a more intuitive design workflow for NEC2 simulations, especially when using [xnec2c](https://github.com/KJ7LNW/xnec2c), which is a free implementation of NEC2 using a GUI.

NEC stands for Numerical Electromagnetics Code and was initially written at Lawrence Livermore Labs in the 1970's, first in Fortran and later translated in C. It is a Method of Moments (MoM) electromagnetics simulator designed for antenna modeling. More information about NEC2 can be found at http://www.nec2.org/.

## Why this project ?

NEC2 (and thus, xnec2c) relies on the original NEC's "punch card deck" format for specifying simulation parameters and geometry. Even if xnec2c tries to simplify this process a bit by making it somehow more visual, I don't like that. I find this a bit archaic and not really intuitive. Documentation on NEC2 cards is also sometimes not really clear for a beginner, too. That's quite a problem, because NEC2 simulations are quick and efficient, which make them useful to figure out some antenna designs or optimize them.

Thus, FenNec2 aims at simplifying this design process by giving more modern, simplified, interface for setting up NEC2 simulations. FenNec2 can then generate NEC2 instruction files, ready to be used by xnec2c for example.

This project takes some bits of this work from Will Snook : [nec2-toys](https://github.com/LordPythonn/nec2-toys/tree/master). The original author of this project wanted to do similar things, and I want to take them a bit further.

In addition, FenNec2 features some tools for making the antenna modeling workflow with xnec2c quicker and easier.

And yes, there is no previous revision of FenNec2. The '2' comes from nothing else than NEC2.

## What will this do ?

The main goal is providing a Python module to generate NEC2-compatible instruction files, mostly using nec2-toys implementation. Useful for parametric modeling. A reimplementation of this old project in a nutshell.

The other main direction of the project will be oriented towards automation with xnec2c. For instance, a quick loading of antenna models, batch simulation, parameter sweeps and optimization are planned.

## How does the Python module work ?

It relies on two main classes: `Model` and `Excitation`.

The `Model` class handles all the geometry stuff, comments and NEC2 file generation.

The `Excitation` class handles, as the name implies, all excitation-related parameters. This is where the simulation frequency range is specified. A `Model` class needs to have an `Excitation` object, which is passed using the `set_excitation()` method.

Just like the original [nec2-toys](https://github.com/LordPythonn/nec2-toys/tree/master), FenNec2 also use geometry helper classes such as `Point` or `Rotation`. It also comes with all the handy functions such as calculating the guided wavelength, unit conversions and more.

For example, here is how we can use FenNec2 to create a simple dipole (`demo.py`) :

```
# Import the needed modules

from nec2utils import *
from physical_constants import *
from units import *

# Some calculations to design a 70 cm band dipole ...
# Remember, frequencies are defined in MHz, lengths in meters

f0 = 435.0
L = wavelength(f0)
z0 = 0.5
segs = 31 # number of segments per wire
correction = 0.95 # dipole length correction factor

# Make a Model and its associated Excitation

m = Model(0.5e-3) # create an antenna using 1 mm diameter wire
exc = Excitation()

# don't care about default values ; we'll define the frequency range with another method
exc.set_frequency_range(420.0, 440.0, 40)
m.set_excitation(exc) # don't forget to link the excitation to the model

# Specify geometry points

a1 = Point(L/4.0*correction, 0, z0)
a2 = Point(-a1.x, 0, z0)

# Link these points into a wire

m.add_wire(segs, a1, a2).feed_at_middle()

# Done, we can now open our model in NEC2 !

m.write_to_file("test.nec")
```

And here is how it looks like when displayed and simulated on xnec2c :

![Our dipole displayed and simulated on xnec2c](./images/dipole_xnec2c.png "Our dipole displayed and simulated on xnec2c").

Please check out the other examples included to see how FenNec2 works.

## How do I install this tool ?

It is recommanded to install FenNec2 for an easier use in your projects or in conjunction with additional tools.

FenNec2 can be easily installed using `pip`, so you should be sure that `pip` is installed with the module `setuptools`.

Then, run the the following command in this folder (eg. where the file `setup.py` is located):

```pip install .```

The installation process has been tested on Linux (Ubuntu 22.04), but it should work on other OS. Installation from PyPi is not yet available but is planned (it will be done when this library will be mature enough).

To upgrade an existing installation, the best way is to use `pip` too:

```pip install --upgrade .```

Same thing for uninstallation:

```pip uninstall fennec2```
