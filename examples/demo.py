# Demonstration file for a simple dipole, just to show the main features and philosophy of FenNec2.


# Import the needed modules

from fennec2 import *
from fennec2.nec2utils import *
from fennec2.physical_constants import *
from fennec2.units import *

# Some calculations to design a 70 cm band dipole ...
# Remember, frequencies are defined in MHz, lengths in meters

f0 = 435.0
L = wavelength(f0)
z0 = 0.5
segs = 31 # number of segments per wire
correction = 0.95 # dipole length correction factor

# Make a Model and its associated Excitation

m = Model(0.5e-3) # create an antenna using 1 mm diameter wire
exc = Excitation()

# don't care about default values ; we'll define the frequency range with another method
exc.set_frequency_range(420.0, 440.0, 40)
m.set_excitation(exc) # don't forget to link the excitation to the model

# Specify geometry points

# DE
a1 = Point(L/4.0*correction, 0, z0)
a2 = Point(-a1.x, 0, z0)

# Link these points into a wire

m.add_wire(segs, a1, a2).feed_at_middle()

# Done, we can now open our model in NEC2 !

m.write_to_file("test.nec")
