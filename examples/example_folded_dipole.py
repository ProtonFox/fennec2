# A slightly more complicated example involving a folded dipole
# Don't forget, its impedance is close to 300 ohms so you better
# set xnec2c to use a 300 ohm feed line

from fennec2 import *

# Some parameters to design a 70 cm band dipole ...
# Remember, frequencies are defined in MHz, lengths in meters

f0 = 435.0               # theorical central frequency
q = wavelength(f0) / 4.0 # quarter of the wavelength
z0 = 1.0                 # height above the ground
gap = 0.05               # the "gap" between the two segments
segs = 31                # number of segments per wire
correction = 0.77        # dipole length correction factor

# Make a Model and its associated Excitation

m = Model(0.5e-3) # create an antenna using 1 mm diameter wire
exc = Excitation()

# don't care about default values ; we'll define the frequency range with another method
exc.set_frequency_range(420.0, 440.0, 40)
m.set_excitation(exc) # don't forget to link the excitation to the model

# lower segment
a1 = Point(q*correction, 0, z0)
a2 = Point(-a1.x, 0, z0)

# upper segment
b1 = Point(q*correction, 0, z0+gap)
b2 = Point(-b1.x, 0, z0+gap)

# geometry helpers
c1 = Point(a1.x, 0, z0+gap/2)  # center of the right-hand arc
c2 = Point(-a1.x, 0, z0+gap/2) # center of the left-hand arc
rot = Rotation(0, 0, 0)        # a "blank" rotation
inv = Rotation(0, 0, 180)      # will invert the left-hand arc

# Link these points into a wire

m.add_wire(segs, b1, b2)
m.add_arc(segs, gap/2, -90, 90, rot, c1)
m.add_arc(segs, gap/2, -90, 90, inv, c2)
m.add_wire(segs, a1, a2).feed_at_middle()

# Done, we can now open our model in NEC2 !

m.write_to_file("../test.nec")
