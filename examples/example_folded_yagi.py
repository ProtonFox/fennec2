# A slight modification of `example_folded_dipole.py` to add some
# parasitic elements allows to turn the folded dipole into a more
# interesting Yagi-Uda antenna for the 70cm band !
# Don't forget, its impedance is close to 300 ohms so you better
# set xnec2c to use a 300 ohm feed line

from fennec2 import *

# Some parameters to design a 70 cm Yagi-Uda antenna with a
# folded dipole driving element ...
# Remember, frequencies are defined in MHz, lengths in meters

f0 = 435.0               # theorical central frequency
q = wavelength(f0) / 4.0 # quarter of the wavelength
z0 = 1.0                 # height above the ground
gap = 0.04               # the "gap" between the two segments
segs = 31                # number of segments per wire
correction = 0.80        # dipole length correction factor

par1 = q * 0.84          # half-length of the first parasitic element
par2 = q * 0.72          # half-length of the second parasitic element
par3 = q * 0.60          # half-length of the third parasitic element

# Make a Model and its associated Excitation

m = Model(1.0e-3) # create an antenna using 2 mm diameter wire
exc = Excitation()

# don't care about default values ; we'll define the frequency range with another method
exc.set_frequency_range(420.0, 440.0, 40)
m.set_excitation(exc) # don't forget to link the excitation to the model

# lower segment
a1 = Point(q*correction, 0, z0)
a2 = Point(-a1.x, 0, z0)

# upper segment
b1 = Point(q*correction, 0, z0+gap)
b2 = Point(-b1.x, 0, z0+gap)

# first parastic element
d1 = Point(par1, 0.9*q, z0+gap/2)
d2 = Point(-d1.x, 0.9*q, z0+gap/2)

# second parasitic element
e1 = Point(par2, 1.75*q, z0+gap/2)
e2 = Point(-e1.x, 1.75*q, z0+gap/2)

# third parasitic element
f1 = Point(par3, 2.5*q, z0+gap/2)
f2 = Point(-f1.x, 2.5*q, z0+gap/2)

# geometry helpers
c1 = Point(a1.x, 0, z0+gap/2)  # center of the right-hand arc
c2 = Point(-a1.x, 0, z0+gap/2) # center of the left-hand arc
rot = Rotation(0, 0, 0)        # a "blank" rotation
inv = Rotation(0, 0, 180)      # will invert the left-hand arc

# Link these points into a wire

m.add_wire(segs, b1, b2)
m.add_wire(segs, d1, d2)
m.add_wire(segs, e1, e2)
m.add_wire(segs, f1, f2)
m.add_arc(segs, gap/2, -90, 90, rot, c1)
m.add_arc(segs, gap/2, -90, 90, inv, c2)
m.add_wire(segs, a1, a2).feed_at_middle()

# Done, we can now open our model in NEC2 !

m.write_to_file("../test.nec")

