# This example shows how to make use of FenNEC2's automation tools to open
# a newly made model in xnec2c in order to get its S-parameters.

from fennec2 import *
from fennec2.nec2utils import *
from fennec2.automation import *
from fennec2.units import *


# We're being a bit lazy, so it's basically the demo file.

f0 = 435.0
L = wavelength(f0)
z0 = 0.5
segs = 31 # number of segments per wire
correction = 0.95 # dipole length correction factor

# An important change! It is recommended to define a model name.
# This the name that will be given to the output files.

m = Model(0.5e-3, "test antenna") # your files will be named "test_antenna"
exc = Excitation()

exc.set_frequency_range(420.0, 440.0, 40)
m.set_excitation(exc) # don't forget to link the excitation to the model

a1 = Point(L/4.0*correction, 0, z0)
a2 = Point(-a1.x, 0, z0)

m.add_wire(segs, a1, a2).feed_at_middle()

# Perform our basic automation action
load_in_xnec2c(m, ".", output_files = [OUTFILE_S1P, OUTFILE_CSV_MEAS])
