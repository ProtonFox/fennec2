# =======================================================================================================
# __init__.py - This file is part of the FenNEC2 project
# Copyright (c) 2024 Proton Fox Electronics (protonthefox@gmail.com)
# =======================================================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following conditions:
# =======================================================================================================
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# =======================================================================================================
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# =======================================================================================================

"""High-level NEC2 Python antenna modeling library with helper tools."""

import math
from fennec2.units import *
from fennec2.nec2utils import *

# =======================================================================================================
# Excitation class
# =======================================================================================================

class Excitation:
	def __init__(self, f_start=100.0, step_size=1.0, step_count=10):
		"""Setup excitation for the defined start frequency, frequency step size and step count.
		In NEC2, frequencies are specified in MHz."""

		self.EX_tag     = 0
		self.EX_segment = 0

		self.f_start    = f_start
		self.step_size  = step_size
		self.step_count = step_count

	def set_frequency_range(self, f_min, f_max, n_steps):
		"""Set the current excitation frequency range f_min, f_max and number of steps"""

		self.f_start = f_min
		self.step_count = n_steps
		self.step_size = (f_max - f_min) / n_steps


# =======================================================================================================
# Model class
# =======================================================================================================

class Model:
	def __init__(self, wireRadius, name="Antenna"):
		"""Prepare the model with the given wire radius"""
		self.name       = name
		self.wires      = ""
		self.transforms = ""
		self.comments   = []
		self.wireRadius = wireRadius
		self.tag        = 0

		self.excitation = None

		self.transformBuffer = ''

	# ---------------------------------------------------------------------------------------------------
	# Low-level functions to generate nec2 cards
	# See documentation at http://www.nec2.org/part_3/cards/
	# Tag & segments have no units. Dimensions are in meters. Angles are in degrees.
	# ---------------------------------------------------------------------------------------------------

	def flush_transform_buffer(self):
		''' Used in some song and dance to avoid the edge case that can occur with an arc as the last element
		    My double GM card trick causes a problem if the second GM tries to refer to a tag that doesn't exist
		'''
		self.transforms      += self.transformBuffer
		self.transformBuffer  = ""


	def gw(self, tag, segments, x1, y1, z1, x2, y2, z2, radius):
		"""Return the line for a GW card, a wire."""
		gw = "GW" + dec(tag) + dec(segments)
		gw += sci(x1) + sci(y1) + sci(z1)
		gw += sci(x2) + sci(y2) + sci(z2)
		gw += sci(radius) + "\n"
		return gw

	def ga(self, tag, segments, arcRadius, startAngle, endAngle, wireRadius):
		"""Return the line for a GA card, an arc in the X-Z plane with its center at the origin"""
		notUsed = 0.0
		ga = "GA" + dec(tag) + dec(segments)
		ga += sci(arcRadius) + sci(startAngle) + sci(endAngle)
		ga += sci(wireRadius)
		ga += sci(notUsed) # Note: xnec2c fills this in with its "Segs % lambda" field, but that may be a bug
		ga += sci(notUsed) + sci(notUsed) + "\n"
		return ga

	def gm(self, rotX, rotY, rotZ, trX, trY, trZ, firstTag):
		""" Return the line for a GM card, move (rotate and translate).
			rotX, rotY, and rotZ: angle to rotate around each axis
			trX, trY, and trZ: distance to translate along each axis
			firstTag: first tag# to apply transform to (subseqent tag#'s get it too... like it or not)
		"""
		tagIncrement = 0
		newStructures = 0
		gm = "GM" + dec(tagIncrement) + dec(newStructures)
		gm += sci(rotX) + sci(rotY) + sci(rotZ)
		gm += sci(trX) + sci(trY) + sci(trZ)
		gm += sci(firstTag*1.0) + "\n"
		return gm

	def ge(self):
		"""Card to "terminate reading of geometry data cards" """
		GPFLAG = 0  # Ground plane flag. 0 means no ground plane present.
		ge = "GE"
		ge += dec(GPFLAG) + "\n"
		return ge

	def fr(self):
		"""Define the frequency range to be modeled"""

		IFRQ = 0                            # Step type, 0 is linear (additive), 1 = multiplicative
		NFRQ = self.excitation.step_count   # Number of frequency steps
		I3   = 0
		I4   = 0
		FMHZ   = self.excitation.f_start    # Starting frequency in MHz
		DELFRQ = self.excitation.step_size  # Frequency stepping increment (IFRQ=0), or multiplication factor (IFRQ=1)

		fr = "FR"
		fr += dec(IFRQ) + dec(NFRQ) + dec(I3) + dec(I4)
		fr += sci(FMHZ) + sci(DELFRQ) + "\n"

		return fr

	def ex(self):
		"""Define excitation parameters."""

		# TODO : all of these parameters are hard-coded ... is this useful to make them configurable ?
		# Maybe within the "Excitation" class to keep things well organized ?

		I1 = 0                          # Excitation type. 0 means an "applied-E-field" voltage source
		I2 = self.excitation.EX_tag     # Tag number of the wire element to which the source will be applied
		I3 = self.excitation.EX_segment # Segment within the previously specified wire element to which the source will be applied

		I4 = 0        # 0 means use defaults for admittance matrix asymmetry and printing input impedance voltage
		F1 = 1.0      # Real part of voltage
		F2 = 0.0      # Imaginary part of voltage
		ex = "EX"

		ex += dec(I1) + dec(I2) + dec(I3) + dec(I4)
		ex += sci(F1) + sci(F2) + "\n"
		return ex


	def rp(self):
		"""Card to initiate calculation and output of radiation pattern."""

		# TODO : all of these parameters are hard-coded ... is this useful to make them configurable ?

		I1  = 0      # 0 is normal mode: defaults to free-space unless a previous GN card specified a ground plane
		NTH = 37     # Number of values of theta (angle away from positive Z axis)
		NPH = 37     # Number of values of phi (angle away from X axis in the XY plane)
		I4  = 0      # Use defaults for some misc output printing options
		THETS = 0.0  # Theta start value in degrees
		PHIS  = 0.0  # Phi start value in degrees
		DTH   = 10.0 # Delta-theta in degrees
		DPH   = 10.0 # Delta-phi in degrees
		rp = "RP"

		rp += dec(I1) + dec(NTH) + dec(NPH) + dec(I4)
		rp += sci(THETS) + sci(PHIS) + sci(DTH) + sci(DPH) + "\n"
		return rp


	def en(self):
		"""Card to mark end of input"""
		return "EN\n"

	# ---------------------------------------------------------------------------------------------------
	# High-level geometry functions
	# ---------------------------------------------------------------------------------------------------

	def add_wire(self, segments, pt1, pt2):
		"""Append a wire, increment the tag number, and return this object to facilitate a chained attachToEX() call"""

		self.tag += 1
		self.wires += self.gw(self.tag, segments, pt1.x, pt1.y, pt1.z, pt2.x, pt2.y, pt2.z, self.wireRadius)
		self.flush_transform_buffer()
		self.middle = math.trunc(segments/2) + 1

		return self

	def add_arc(self, segments, radius, start, end, rotate, translate):
		""" Append an arc using a combination of a GA card (radius, start angle, end angle), a GM card to rotate
			and translate the arc from the origin into it's correct location, and a second GM card to restore the
			transformation matrix for cards that come after the arc.
		"""

		# Place the arc in the XZ plane with its center on the origin
		self.tag += 1
		self.wires += self.ga(self.tag, segments, radius, start, end, self.wireRadius)
		self.flush_transform_buffer()
		self.middle = math.trunc(segments/2) + 1

		# Move the arc to where it's supposed to be (note the tag #)
		r = rotate
		t = translate
		self.transforms += self.gm(r.rx, r.ry, r.rz, t.x, t.y, t.z, self.tag)

		# Queue up the transforms to roll back the translation and rotation, using multiple gm cards to ensure
		# that it really works (see GM card documentation about order of operations). This will restore the normal
		# coordinate system if any elements are appended to the model after this arc, but the use of tag = n+1
		# means it could break the nec2 parser if it's included without a GW or GA that actually uses tag n+1. The
		# point of this buffering nonsense is to avoid triggering that parsing problem.

		self.transformBuffer += self.gm(  0.0,   0.0,   0.0, -t.x, -t.y, -t.z, self.tag+1)
		self.transformBuffer += self.gm(  0.0,   0.0, -r.rz,  0.0,  0.0,  0.0, self.tag+1)
		self.transformBuffer += self.gm(  0.0, -r.ry,   0.0,  0.0,  0.0,  0.0, self.tag+1)
		self.transformBuffer += self.gm(-r.rx,   0.0,   0.0,  0.0,  0.0,  0.0, self.tag+1)

		return self

	def feed_at_middle(self):
		"""Attach the EX card feedpoint to the middle segment of the element that was most recently created"""
		self.excitation.EX_tag     = self.tag
		self.excitation.EX_segment = self.middle

	# ---------------------------------------------------------------------------------------------------
	# Excitation settings & File I/O
	# ---------------------------------------------------------------------------------------------------
	
	def set_model_name(self, name):
		"""Set the model's name. Used for the output files' names."""
		self.name = name
	
	def get_model_name(self):
		"""Return the model's name."""
		return self.name

	def set_excitation(self, exc):
		"""Set the current excitation to be associated with the current model"""
		self.excitation = exc


	def getText(self):
		"""Generates the NEC2 cards from the model and excitation settings"""

		if self.excitation != None:
			footer = self.ge()
			footer += self.ex()
			footer += self.fr()
			footer += self.rp()
			footer += self.en()
			return self.wires + self.transforms + footer
		else:
			print("ERROR: an Excitation object must be defined first")
			return ""


	def format_comments(self):
		"""Processes comment lines in NEC2 format"""

		# TODO : specify FenNec2 version automatically ?
		output = "CM NEC2 model file generated with FenNec2\n"

		for line in self.comments:
			output += ("CM " + line.strip() + "\n")

		output += "CE"

		return output


	def add_comment(self, comment):
		"""Add one comment line to the current model"""

		self.comments.append(comment)


	def write_to_file(self, path):
		"""Writes the current model to a .nec file."""

		with open(path, 'w') as nec_file:
			nec_file.write(self.format_comments() + "\n")
			nec_file.write(self.getText().strip() + "\n")

			nec_file.close()

