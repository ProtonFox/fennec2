# =======================================================================================================
# automation.py - This file is part of the FenNEC2 project
# Copyright (c) 2024 Proton Fox Electronics (protonthefox@gmail.com)
# =======================================================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following conditions:
# =======================================================================================================
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# =======================================================================================================
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# =======================================================================================================

"""Xnec2c automation tools. Xnec2c must be installed and working for using this module.""" 

import fennec2 as nec
import subprocess
from os.path import join

# Output files options for Xnec2c. They correspond to the existing `--write-nnn` options.
OUTFILE_S1P             = 0 # Return Loss in a 1-port Touchstone file.
OUTFILE_S2P_MAX_GAIN    = 1 # Max gain in a 2-port Touchstone file.
OUTFILE_S2P_VIEWER_GAIN = 2 # Gain in viewer's direction in a 2-port Touchstone file.
OUTFILE_CSV_RADPAT      = 3 # Radiation pattern in a CSV file.
OUTFILE_CSV_CURRENTS    = 4 # Current distribution in a CSV file.
OUTFILE_CSV_MEAS        = 5 # A lot of data, from impedance to radiation pattern-related metrics.

def load_in_xnec2c(model, path, batch=True, output_files=[OUTFILE_S1P]):
    """Automation function to run a FenNEC2 model into xnec2c, batch mode by default.
    Simulation results are saved in a s1p file in the specified path."""
    
    # create paths and output files names
    nec_file = join(path, model.get_model_name() + ".nec")
    systcall = ["xnec2c"]
    
    # create the subprocess.run() list of arguments
    if batch:
        systcall.append("--batch")
        
    systcall.append("-i")
    systcall.append(nec_file)
    
    for file_opt in output_files:
        if file_opt == OUTFILE_S1P:
            systcall.append("--write-s1p")
            systcall.append(join(path, model.get_model_name() + ".s1p"))
        elif file_opt == OUTFILE_S2P_MAX_GAIN:
            systcall.append("--write-s2p-max-gain")
            systcall.append(join(path, model.get_model_name() + "_max_gain.s2p"))
        elif file_opt == OUTFILE_S2P_VIEWER_GAIN:
            systcall.append("--write-s2p-viewer-gain")
            systcall.append(join(path, model.get_model_name() + "_viewer_gain.s2p"))
        elif file_opt == OUTFILE_CSV_RADPAT:
            systcall.append("--write-rdpat")
            systcall.append(join(path, model.get_model_name() + "_radiation.csv"))
        elif file_opt == OUTFILE_CSV_CURRENTS:
            systcall.append("--write-currents")
            systcall.append(join(path, model.get_model_name() + "_currents.csv"))
        elif file_opt == OUTFILE_CSV_MEAS:
            systcall.append("--write-csv")
            systcall.append(join(path, model.get_model_name() + "_meas.csv"))
    
    model.write_to_file(nec_file)

    subprocess.run(systcall)

