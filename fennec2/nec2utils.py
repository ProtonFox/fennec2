# =======================================================================================================
# nec2utils.py - This file is part of the FenNEC2 project
# Copyright (c) 2024 Proton Fox Electronics (protonthefox@gmail.com)
# =======================================================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following conditions:
# =======================================================================================================
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# =======================================================================================================
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# =======================================================================================================

"""NEC2 model edition utilities."""

import math


# =======================================================================================================
# Field formatting functions (i.e. "columns" in punchcard-speak)
# =======================================================================================================

def sci(f):
	"""Return formatted string containing a scientific notation float in a 13 char wide field (xyz coordiates, radius)"""
	return '{: > 13.5E}'.format(f)


def dec(i):
	"""Return formatted string containing a decimal integer in a 6 char wide field (tags, segments)"""
	return '{: >6d}'.format(math.trunc(i))


# =======================================================================================================
# 3D point and rotation classes
# =======================================================================================================

class Point:
	def __init__(self,x,y,z):
		"""Class for a 3D point/vector. Coordinates in meters."""
		self.x = float(x)
		self.y = float(y)
		self.z = float(z)


class Rotation:
	def __init__(self,rx,ry,rz):
		"""Class for a 3D rotation helper. Angles in degrees."""
		self.rx = float(rx)
		self.ry = float(ry)
		self.rz = float(rz)


# =======================================================================================================
# File I/O
# =======================================================================================================

def dump_file_to_console(fileName):
	"""Dump the card stack back to the console for a quick sanity check"""
	nec2File = open(fileName,'r')
	print(nec2File.read())
	nec2File.close()
