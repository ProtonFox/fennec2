# =======================================================================================================
# units.py - This file is part of the FenNEC2 project
# Copyright (c) 2024 Proton Fox Electronics (protonthefox@gmail.com)
# =======================================================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following conditions:
# =======================================================================================================
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# =======================================================================================================
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# =======================================================================================================

"""
Physical measurements processing and unit conversion functions.
Unit conversion functions from the original nec2-toys project. As they would almost exclusively be used
by imperial units users, they all have been put in this file.
"""

from fennec2.physical_constants import *
from math import sqrt

# FenNec2 additions.

# =======================================================================================================
# Wavelength calculations and related stuff
# =======================================================================================================

def wavelength(frequency_MHz, e_r = 1.0, mu_r = 1.0):
	"""Returns the guided wavelength, in meters, for a frequency given in MHz.
	Medium relative permittivity and permeability can be specified if needed.
	Default is for vacuum."""

	return C0 / (frequency_MHz * 1e6 * sqrt(e_r * mu_r))

def lambda_to_m(L, frequency_MHz, e_r = 1.0, mu_r = 1.0):
	"""Returns the distance in meters associated to the number of wavelengths L
	at a given frequency.
	Medium relative permittivity and permeability can be specified if needed.
	Default is for vacuum."""

	wl = wavelength(frequency_MHz, e_r, mu_r)
	return L * wl

# From the original nec2-toys project with some slight code style adjustments.

# =======================================================================================================
# Unit conversions... The nec2 engine requires its inputs to be in meters and degrees. Note that these
# functions are named to denote the pre-conversion units, because I consider those more suitable for
# the calculations I will be working with.
# =======================================================================================================

def m(m):
	"""Convert meters to meters. Useful for being consistent about always specifying units and for
	making sure not to accidentaly run afoul of Python's integer math (hence the * 1.0)"""
	return m * 1.0

def inch(i):
	"""Convert inches to meters"""
	return i * 2.54 / 100.0

def deg(degrees):
	"""Make sure degrees are float"""
	return degrees * 1.0

# =======================================================================================================
# Output conversions from meters back to inches
# =======================================================================================================

def mToIn(meters):
	"""Convert meters back to inches for output in the comment section"""
	return meters * 100.0 / 2.54
