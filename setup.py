# WIP but should work

from setuptools import setup

setup(
    name='fennec2',
    version='0.1.0',
    description='A high-level NEC2 antenna modeler.',
    url='https://gitlab.com/ProtonFox/fennec2',
    author='ProtonFox (F4JTE)',
    author_email='protonthefox@gmail.com',
    license='MIT License',
    packages=['fennec2'],
    install_requires=['numpy',
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.11',
    ],
)

